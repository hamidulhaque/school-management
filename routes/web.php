<?php

use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\GuardianDashboardController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentDashboardController;
use App\Http\Controllers\TeacherDashboardController;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\TransactionController;
use App\Models\Payment;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\CustomLoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/





// login and recovery routes

Route::middleware(['web'])->group(function () {

    Route::get('/', [CustomLoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login', [CustomLoginController::class, 'login'])->name('login.check');
    Route::post('/logout', [CustomLoginController::class, 'logout'])->name('logout');

});
Route::prefix('/sign-up')->group(function () {

});

// open routes 


//common auth routes
Route::group(['middleware' => ['auth']], function () {
    Route::post('/notice-clear', [NoticeController::class, 'clearNoticeTop'])->name('notice.clear');
    Route::get('/notices/', [NoticeController::class, 'noticeViewAll'])->name('notice.all');
    Route::get('/notice/{slug}', [NoticeController::class, 'noticeViewSingle'])->name('notice.view');
});







//ADMIN ROUTES
Route::group(['middleware' => ['auth', 'role:admin']], function () {
    Route::prefix('/admin')->group(function () {
        Route::get('/dashboard', [AdminDashboardController::class, 'dashboard'])->name('admin.dashboard');

        //class web routes 
        Route::controller(SchoolClassController::class)->group(function () {
            Route::prefix('/classes')->group(function () {
                Route::get('/', 'classes')->name('admin.class.index');
                Route::get('/create', 'create')->name('admin.class.create');
                Route::post('/create', 'store')->name('admin.class.store');
                Route::get('/edit/{class}', 'edit')->name('admin.class.edit');
                Route::patch('/edit/{class}', 'update')->name('admin.class.update');
                Route::delete('/delete/{class}', 'destroy')->name('admin.class.destroy');
            });

        });

        //section web routes 
        Route::controller(SectionController::class)->group(function () {
            Route::prefix('/sections')->group(function () {
                Route::get('/', 'index')->name('admin.section.index');
                Route::get('/create', 'create')->name('admin.section.create');
                Route::post('/create', 'store')->name('admin.section.store');
                Route::get('/edit/{section}', 'edit')->name('admin.section.edit');
                Route::patch('/edit/{section}', 'update')->name('admin.section.update');
                Route::delete('/delete/{section}', 'destroy')->name('admin.section.destroy');

                Route::get('/add-students/{section}', 'addStu')->name('admin.section.addStu');
                Route::post('/add-students/{section}/store', 'addStuStore')->name('admin.section.addStuStore');

                Route::get('/add-teachers/{section}', 'addTeacher')->name('admin.section.addTeacher');
                Route::post('/add-teachers/{section}/store', 'addTeacherStore')->name('admin.section.addTeacherStore');
            });

        });

        //notices web routes
        Route::controller(NoticeController::class)->group(function () {
            Route::prefix('/notice')->group(function () {
                Route::get('/', 'index')->name('admin.notice.index');
                Route::get('/create', 'create')->name('admin.notice.create');
                Route::post('/create', 'store')->name('admin.notice.store');
                Route::get('/edit/{notice}', 'edit')->name('admin.notice.edit');
                Route::patch('/edit/{notice}', 'update')->name('admin.notice.update');
                Route::delete('/delete/{notice}', 'destroy')->name('admin.notice.destroy');
            });
        });

        //accounts web routes
        Route::controller(PaymentController::class)->group(function () {
            Route::prefix('/payment')->group(function () {
                Route::get('/', 'adminPayment')->name('admin.payment.index');
                Route::post('/search', 'adminStuSearch')->name('admin.payment.search.stu');
                Route::post('/payment-store', 'adminPaymentStore')->name('admin.payment.store');
                Route::get('/delete/{student_id}/{transactionNumber}', 'adminPayDelete')->name('admin.payment.delete');
            });
        });



        Route::controller(TransactionController::class)->group(function () {
            Route::prefix('/transactions')->group(function () {
                Route::get('/', 'adminTransactionView')->name('admin.transaction.index');

            });
        });

        Route::controller(StudentController::class)->group(function () {
            Route::prefix('/students')->group(function () {
                Route::get('/pending', 'adminStudentAppList')->name('admin.student.pending');
                Route::get('/create', 'adminStuCreate')->name('admin.student.create');
                Route::post('/create', 'adminStuStore')->name('admin.student.store');


            });
        });



    });
});



//STUDENT ROUTES
Route::group(['middleware' => ['auth', 'role:student']], function () {
    Route::prefix('/student')->group(function () {
        Route::get('/dashboard', [StudentDashboardController::class, 'dashboard'])->name('student.dashboard');
    });
});





//TEACHER ROUTES
Route::group(['middleware' => ['auth', 'role:teacher']], function () {
    Route::prefix('/teacher')->group(function () {
        Route::get('/dashboard', [TeacherDashboardController::class, 'dashboard'])->name('teacher.dashboard');
    });
});




//GUARDIAN ROUTES
Route::group(['middleware' => ['auth', 'role:guardian']], function () {
    Route::prefix('/guardian')->group(function () {
        Route::get('/dashboard', [GuardianDashboardController::class, 'dashboard'])->name('guardian.dashboard');
    });
});

