<x-frontend.layouts.master>
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-4">
                <div class="text-center mb-3">
                    <a href="index.html" class="">
                        <img src="{{ asset('backend/images/logo-dark.png') }}" alt="" height="22"
                            class="mx-auto">
                    </a>

                </div>


                <div class="card">
                    <div class="card-body p-4">

                        <div class="text-center mb-4">
                            <h4 class="text-uppercase mt-0">Sign In</h4>
                        </div>

                        <form action="{{ route('login.check') }}" class="form-prevent" method="POST">
                            @csrf

                            <div class="mb-3">
                                <label for="identifier" class="form-label">ID/Phone/Email</label>
                                <input class="form-control" type="text" id="identifier" name="identifier"
                                    value="{{ old('identifier') }}" required placeholder="Enter your id/email">
                                @if ($errors->has('identifier'))
                                    <span class="text-danger">{{ $errors->first('identifier') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input class="form-control" type="password" id="password" name="password" required
                                    placeholder="Enter your password">
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="checkbox-signin" name="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="checkbox-signin">Remember me</label>
                                </div>
                            </div>

                            <div class="mb-3 d-grid text-center">
                                <button class="btn btn-primary form-prevent-multiple-submit" type="submit"> Log In </button>
                            </div>
                        </form>

                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p> <a href="pages-recoverpw.html" class="text-muted ms-1"><i class="fa fa-lock me-1"></i>Forgot
                                your password?</a></p>
                        <p class="text-muted">Don't have an account? <a href="pages-register.html"
                                class="text-dark ms-1"><b>Sign Up</b></a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</x-frontend.layouts.master>
