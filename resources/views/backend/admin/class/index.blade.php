<x-backend.layouts.master>
    @section('page-title', 'Classes/')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Running Classes</h4>
                        <a href="{{ route('admin.class.create') }}" class="btn btn-primary">Add New Class</a>
                    </div>

                    <section class="mt-3">
                        <div class="row">
                            <div class="col-12">

                                <table id="datatable"
                                    class="table table-bordered dt-responsive table-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>Academic Year</th>
                                            <th>Last Enrollment</th>
                                            <th>Total Payment</th>
                                            <th>Duration (Months)</th>
                                            <th>Max Students</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($classes)
                                            @foreach ($classes as $class)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $class->name }}</td>
                                                    <td>{{ $class->start_date }}</td>
                                                    <td>{{ $class->academic_year }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($class->last_enrollment)->format('d-m-Y') }}
                                                    </td>
                                                    <td>{{ $class->total_payment }}</td>
                                                    <td>{{ $class->duration_months }}</td>
                                                    <td>{{ $class->max_student }}</td>
                                                    <td>{{ $class->created_at }}</td>
                                                    <td><a href="{{ route('admin.class.edit', $class->id) }}"
                                                            class="btn btn-sm btn-outline-primary rounded">Edit</a>
                                                        <a href="{{ route('admin.class.destroy', $class->id) }}"
                                                            class="btn btn-sm btn-outline-danger rounded"
                                                            onclick="javascript:return confirm('Are you sure you want to delete this?')">Delete</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @endif
                                    </tbody>

                                </table>

                            </div>
                        </div>

                    </section>

                    {{-- 
                    <div class="row">
                        <div class="col-lg-6">
                            <form>

                                <div class="mb-3">
                                    <label for="simpleinput" class="form-label">Text</label>
                                    <input type="text" id="simpleinput" class="form-control">
                                </div>

                                <div class="mb-3">
                                    <label for="example-email" class="form-label">Email</label>
                                    <input type="email" id="example-email" name="example-email" class="form-control"
                                        placeholder="Email">
                                </div>

                                <div class="mb-3">
                                    <label for="example-password" class="form-label">Password</label>
                                    <input type="password" id="example-password" class="form-control" value="password">
                                </div>

                                <div class="mb-3">
                                    <label for="password" class="form-label">Show/Hide Password</label>
                                    <div class="input-group input-group-merge">
                                        <input type="password" id="password" class="form-control"
                                            placeholder="Enter your password">
                                        <div class="input-group-text" data-password="false">
                                            <span class="password-eye"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="example-palaceholder" class="form-label">Placeholder</label>
                                    <input type="text" id="example-palaceholder" class="form-control"
                                        placeholder="placeholder">
                                </div>

                                <div class="mb-3">
                                    <label for="example-textarea" class="form-label">Text area</label>
                                    <textarea class="form-control" id="example-textarea" rows="5"></textarea>
                                </div>

                                <div class="mb-3">
                                    <label for="example-readonly" class="form-label">Readonly</label>
                                    <input type="text" id="example-readonly" class="form-control" readonly=""
                                        value="Readonly value">
                                </div>

                                <div class="mb-3">
                                    <label for="example-disable" class="form-label">Disabled</label>
                                    <input type="text" class="form-control" id="example-disable" disabled=""
                                        value="Disabled value">
                                </div>

                                <div class="mb-3">
                                    <label for="example-static" class="form-label">Static control</label>
                                    <input type="text" readonly="" class="form-control-plaintext"
                                        id="example-static" value="email@example.com">
                                </div>

                                <div>
                                    <label for="example-helping" class="form-label">Helping text</label>
                                    <input type="text" id="example-helping" class="form-control"
                                        placeholder="Helping text">
                                    <span class="help-block"><small>A block of help text that breaks onto a new line and
                                            may extend beyond one line.</small></span>
                                </div>

                            </form>
                        </div> <!-- end col -->

                        <div class="col-lg-6">
                            <form>

                                <div class="mb-3">
                                    <label for="example-select" class="form-label">Input Select</label>
                                    <select class="form-select" id="example-select">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label for="example-multiselect" class="form-label">Multiple Select</label>
                                    <select id="example-multiselect" multiple="" class="form-select">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label for="example-fileinput" class="form-label">Default file input</label>
                                    <input type="file" id="example-fileinput" class="form-control">
                                </div>

                                <div class="mb-3">
                                    <label for="example-date" class="form-label">Date</label>
                                    <input class="form-control" id="example-date" type="date" name="date">
                                </div>

                                <div class="mb-3">
                                    <label for="example-month" class="form-label">Month</label>
                                    <input class="form-control" id="example-month" type="month" name="month">
                                </div>

                                <div class="mb-3">
                                    <label for="example-time" class="form-label">Time</label>
                                    <input class="form-control" id="example-time" type="time" name="time">
                                </div>

                                <div class="mb-3">
                                    <label for="example-week" class="form-label">Week</label>
                                    <input class="form-control" id="example-week" type="week" name="week">
                                </div>

                                <div class="mb-3">
                                    <label for="example-number" class="form-label">Number</label>
                                    <input class="form-control" id="example-number" type="number" name="number">
                                </div>

                                <div class="mb-3">
                                    <label for="example-color" class="form-label">Color</label>
                                    <input class="form-control" id="example-color" type="color" name="color"
                                        value="#71b6f9">
                                </div>

                                <div>
                                    <label for="example-range" class="form-label">Range</label>
                                    <input class="form-range" id="example-range" type="range" name="range"
                                        min="0" max="100">
                                </div>

                            </form>
                        </div> <!-- end col -->
                    </div> --}}
                    <!-- end row-->

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>


    @push('js')
        <script src="{{ asset('backend/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/pdfmake.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/vfs_fonts.js') }}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ asset('backend/js/pages/datatables.init.js') }}"></script>
    @endpush



</x-backend.layouts.master>
