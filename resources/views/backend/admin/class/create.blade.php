<x-backend.layouts.master>
    @section('page-title', 'Classes/ Create')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Add new class</h4>
                        <a href="{{ route('admin.class.index') }}" class="btn btn-primary">All Classes</a>
                    </div>

                    <form action="{{ route('admin.class.store') }}" class="form-prevent" method="POST">
                        @csrf
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Class Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}" placeholder="Enter class name" required>
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <label for="start_date" class="form-label">Start Date</label>
                                    <input type="date" class="form-control" id="start_date" name="start_date"
                                        value="{{ old('start_date') }}" placeholder="Enter Start Date" required>
                                    @error('start_date')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="academic_year" class="form-label">Academic Year</label>
                                    <input type="number" class="form-control" id="academic_year" name="academic_year"
                                        value="{{ old('academic_year') }}" placeholder="Enter academic year" required>
                                    @error('academic_year')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <label for="last_enrollment" class="form-label">Last Enrollment Date</label>
                                    <input type="date" class="form-control" id="last_enrollment"
                                        name="last_enrollment" value="{{ old('last_enrollment') }}"
                                        placeholder="Enter last enrollment date">
                                    @error('last_enrollment')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="total_payment" class="form-label">Total Payment</label>
                                    <input type="number" class="form-control" id="total_payment" name="total_payment"
                                        value="{{ old('total_payment') }}" placeholder="Enter total payment"
                                        step="0.01" required>
                                    @error('total_payment')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <label for="duration_months" class="form-label">Duration (Months)</label>
                                    <input type="number" class="form-control" id="duration_months"
                                        name="duration_months" value="{{ old('duration_months') }}"
                                        placeholder="Enter duration in months" required>
                                    @error('duration_months')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <label for="max_student" class="form-label">Max Students</label>
                                    <input type="number" class="form-control" id="max_student" name="max_student"
                                        value="{{ old('max_student') }}" placeholder="Enter max students" required>
                                    @error('max_student')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit"
                                    class="btn btn-primary form-prevent-multiple-submit w-100">Submit</button>
                            </div>
                        </div>
                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>
</x-backend.layouts.master>
