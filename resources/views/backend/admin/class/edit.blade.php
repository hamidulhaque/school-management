<x-backend.layouts.master>
    @section('page-title', 'Classes/ Edit')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Edit class</h4>
                        <a href="{{ route('admin.class.index') }}" class="btn btn-primary">All Classes</a>
                    </div>

                    <form action="{{ route('admin.class.update', $class->id) }}" method="POST" class="form-prevent">
                        @csrf
                        @method('PATCH')
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Class Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name', $class->name) }}" required placeholder="Enter class name">
                                    @if ($errors->has('name'))
                                        <div class="text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="academic_year" class="form-label">Academic Year</label>
                                    <input type="number" class="form-control" id="academic_year" name="academic_year"
                                        value="{{ old('academic_year', $class->academic_year) }}" required
                                        placeholder="Enter academic year">
                                    @if ($errors->has('academic_year'))
                                        <div class="text-danger">{{ $errors->first('academic_year') }}</div>
                                    @endif
                                </div>
                                <div class="mb-3">
                                    <label for="start_date" class="form-label">Start Date</label>
                                    <input type="date" class="form-control" id="start_date" name="start_date"
                                        value="{{ old('start_date', $class->start_date) }}"
                                        placeholder="Enter Start Date" required>
                                    @error('start_date')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="last_enrollment" class="form-label">Last Enrollment Date</label>
                                    <input type="date" class="form-control" id="last_enrollment"
                                        name="last_enrollment"
                                        value="{{ old('last_enrollment', $class->last_enrollment) }}">
                                    @if ($errors->has('last_enrollment'))
                                        <div class="text-danger">{{ $errors->first('last_enrollment') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="total_payment" class="form-label">Total Payment</label>
                                    <input type="number" class="form-control" id="total_payment" name="total_payment"
                                        value="{{ old('total_payment', $class->total_payment) }}" step="0.01"
                                        required placeholder="Enter total payment">
                                    @if ($errors->has('total_payment'))
                                        <div class="text-danger">{{ $errors->first('total_payment') }}</div>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="duration_months" class="form-label">Duration (Months)</label>
                                    <input type="number" class="form-control" id="duration_months"
                                        name="duration_months"
                                        value="{{ old('duration_months', $class->duration_months) }}" required
                                        placeholder="Enter duration in months">
                                    @if ($errors->has('duration_months'))
                                        <div class="text-danger">{{ $errors->first('duration_months') }}</div>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <label for="max_student" class="form-label">Max Students</label>
                                    <input type="number" class="form-control" id="max_student" name="max_student"
                                        value="{{ old('max_student', $class->max_student) }}"
                                        placeholder="Enter max students">
                                    @if ($errors->has('max_student'))
                                        <div class="text-danger">{{ $errors->first('max_student') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit"
                                    class="btn btn-primary form-prevent-multiple-submit w-100">Submit</button>
                            </div>
                        </div>
                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>
</x-backend.layouts.master>
