<x-backend.layouts.master>
    @section('page-title', 'Student Create!')



    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Create a new student
                        </h4>
                    </div>



                    @isset($success)
                        <div class="row col-12 col-md-12 col-sm-12 mb-2">
                            <div class="col-md-6">
                                <div class="alert alert-success" role="alert">
                                    <h4 class="alert-heading">Student Credentials:</h4>
                                    <p class="m-0 p-0 pb-1">
                                        <span>Student ID: {{ $studentCredentials['student_id'] }}</span>

                                    </p>
                                    <p class="m-0 p-0 pb-1">
                                        <span>Password: {{ $studentCredentials['password'] }}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="alert alert-success" role="alert">
                                    <h4 class="alert-heading">Guardian Credentials:</h4>
                                    <p class="m-0 p-0 pb-1">
                                        <span>Username: {{ $guardianCredentials['username'] }}</span>
                                    </p>
                                    <p class="m-0 p-0 pb-1">
                                        <span>Password: {{ $guardianCredentials['password'] }}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endisset




                    <div class="row col-12 col-md-12 col-sm-12">
                        <form action="{{ route('admin.student.store') }}" class="form-prevent" method="POST">
                            @csrf
                            <div class="row mt-3">
                                <!-- Student Information -->
                                <div class="col-md-6">
                                    <h4>Student Information</h4>
                                    <div class="mb-3">
                                        <label for="student_name" class="form-label">Student Name</label>
                                        <input class="form-control" id="student_name" name="student_name"
                                            placeholder="Enter student name" value="{{ old('student_name') }}"
                                            required />
                                        @error('student_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="class_id" class="form-label">Class</label>
                                        <select class="form-control form-select" id="class_id" name="class_id"
                                            required>
                                            <option value="">Select Class</option>
                                            @foreach ($classes as $class)
                                                <option value="{{ $class->id }}"
                                                    {{ old('class_id') == $class->id ? 'selected' : '' }}>
                                                    {{ $class->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('class_id')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="section_id" class="form-label">Section</label>
                                        <select class="form-control  form-select" id="section_id" name="section_id">
                                            <option value="">Select Section</option>
                                            @foreach ($sections as $section)
                                                <option value="{{ $section->id }}"
                                                    {{ old('section_id') == $section->id ? 'selected' : '' }}>
                                                    {{ $section->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('section_id')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Guardian Information -->
                                <div class="col-md-6">
                                    <h4>Guardian Information</h4>
                                    <div class="mb-3">
                                        <label for="guardian_name" class="form-label">Guardian Name</label>
                                        <input class="form-control" id="guardian_name" name="guardian_name"
                                            placeholder="Enter guardian name" value="{{ old('guardian_name') }}"
                                            required />
                                        @error('guardian_name')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="guardian_phone" class="form-label">Guardian Phone</label>
                                        <input class="form-control" id="guardian_phone" name="guardian_phone"
                                            placeholder="Enter guardian phone" value="{{ old('guardian_phone') }}"
                                            required />
                                        @error('guardian_phone')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit"
                                    class="btn btn-primary form-prevent-multiple-submit w-100">Submit</button>
                            </div>
                        </form>
                    </div>



                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>

</x-backend.layouts.master>
