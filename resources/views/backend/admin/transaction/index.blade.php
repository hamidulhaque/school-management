<x-backend.layouts.master>
    @section('page-title', 'Recent Transactions')

    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="d-flex justify-content-between align-items-center mb-4">
                    <h4 class="header-title">Recent Transactions</h4>
                </div>

                <div class="col-12 col-md-12 col-sm-12">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <div>
                            <form action="{{ route('admin.transaction.index') }}" method="GET" class="d-flex">
                                <input type="search" name="search" class="form-control me-2" placeholder="Search"
                                    title="Search using student id or transaction number"
                                    value="{{ request('search') }}">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>
                        <div>
                            <form action="{{ route('admin.transaction.index') }}" method="GET" class="d-flex">
                                <div class="d-flex align-items-center">
                                    <label for="limit" class="me-2">Show</label>
                                    <select name="limit" id="limit" class="form-select"
                                        onchange="this.form.submit()">
                                        <option value="5"{{ request('limit') == 5 ? ' selected' : '' }}>5</option>
                                        <option value="10"{{ request('limit') == 10 ? ' selected' : '' }}>10
                                        </option>
                                        <option value="20"{{ request('limit') == 20 ? ' selected' : '' }}>20
                                        </option>
                                        <option value="50"{{ request('limit') == 50 ? ' selected' : '' }}>50
                                        </option>
                                        <option value="100"{{ request('limit') == 100 ? ' selected' : '' }}>100
                                        </option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 mb-2">
                    <table class="table dt-responsive table-responsive nowrap align-middle">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Amount</th>
                                <th>Student ID</th>
                                <th>Transaction ID</th>
                                <th>At</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php
                                $i = ($transactions->currentPage() - 1) * $transactions->perPage() + 1;
                            @endphp
                            @forelse ($transactions as $transaction)
                                <tr>
                                    {{-- <td>{{ $loop->iteration }}</td> --}}
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $transaction->amount_paid }}</td>
                                    <td>{{ $transaction->student->student_id }}</td>
                                    <td>{{ $transaction->transaction_number }}</td>
                                    <td>{{ $transaction->created_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">No history found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <!-- Pagination Links -->
                    <div class="d-flex justify-content-center">
                        {{ $transactions->appends(['limit' => $limit, 'search' => $search])->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-backend.layouts.master>
