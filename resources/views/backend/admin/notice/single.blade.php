<x-backend.layouts.master>
    
    @section('page-title', "Notices/ {$notice->slug}")
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <p class="header-title fw-semibold" style="font-family: Karla, Bangla605, sans-serif;">{{ $notice->title }}
                        </p>
                        <p class="header-title">{{ $notice->created_at->diffForHumans() }}</p>
                        
                    </div>


                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body ">
                    <p class="mb-0">{{ $notice->content }}</p>
                </div>
            </div>
        </div>
    </div>


  



</x-backend.layouts.master>
