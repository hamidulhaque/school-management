<x-backend.layouts.master>
    @section('page-title', 'Notices/')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Recent Notices</h4>
                        <a href="{{ route('admin.notice.create') }}" class="btn btn-primary">New Notices</a>
                    </div>

                    <section class="mt-3">
                        <div class="row">
                            <div class="col-12">
                                <table id="datatable"
                                    class="table table-bordered dt-responsive table-responsive nowrap align-middle" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($notices)

                                            @foreach ($notices as $notice)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>

                                                    <td>{{ $notice->title }}</td>
                                                    <td>{{ Str::limit($notice->content, 40, '...') }}</td>
                                                    <td>{{ $notice->created_at }}</td>

                                                    <td>
                                                        <a href="{{ route('admin.notice.edit', $notice->id) }}"
                                                            class="btn btn-sm btn-outline-primary rounded">View /
                                                            Edit</a>


                                                        <a href="{{ route('admin.notice.destroy', $notice->id) }}"
                                                            class="btn btn-sm btn-outline-danger rounded"
                                                            onclick="javascript:return confirm('Are you sure you want to delete this?')">Delete</a>
                                                    </td>

                                                </tr>
                                            @endforeach

                                        @endif
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </section>
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>


    @push('js')
        <script src="{{ asset('backend/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/pdfmake.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/vfs_fonts.js') }}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ asset('backend/js/pages/datatables.init.js') }}"></script>
    @endpush



</x-backend.layouts.master>
