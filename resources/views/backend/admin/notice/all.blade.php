<x-backend.layouts.master>

    @section('page-title', 'All Notices')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h4 class="header-title">All Notices</h4>

                    </div>

                    @if ($notices->count())
                        <table id="noticesTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($notices as $notice)
                                    <tr>
                                        <td>{{ $notice->id }}</td>
                                        <td>{{ $notice->title }}</td>
                                        <td>{{ Str::limit($notice->content, 50) }}</td>
                                        <td>{{ $notice->created_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="{{ route('notice.view', $notice->slug) }}"
                                                class="btn btn-outline-primary btn-sm">View</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No records found!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <!-- Pagination links -->
                        <div class="d-flex justify-content-center">
                            {{ $notices->onEachSide(5)->links() }}
                            {{-- {{ $notices->links('pagination::bootstrap-4')}} --}}

                        </div>
                    @else
                        <p class="text-center">No notices found</p>
                    @endif

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>


</x-backend.layouts.master>
