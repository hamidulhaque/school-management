<x-backend.layouts.master>
    @section('page-title', 'Edit Notice')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Edit Notice</h4>
                        <a href="{{ route('admin.notice.index') }}" class="btn btn-primary">All Notices</a>
                    </div>

                    <form action="{{ route('admin.notice.update', $notice->id) }}" method="POST" class="form-prevent">
                        @csrf
                        @method('PATCH')
                        <div class="row mt-3">


                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="content" class="form-label">Notice Title</label>
                                    <input class="form-control" id="tilte" name="title" placeholder="Enter notice title" value="{{ old('title', $notice->title) }}" required/>
                                    @error('title')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label for="content" class="form-label">Notice Content</label>
                                    <textarea class="form-control" id="content" name="content" placeholder="Enter notice content" required>{{ old('content', $notice->content) }}</textarea>
                                    @error('content')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary form-prevent-multiple-submit w-100">Update Notice</button>
                        </div>
                    </form>

                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>
</x-backend.layouts.master>
