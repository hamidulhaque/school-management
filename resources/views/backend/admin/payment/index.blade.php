<x-backend.layouts.master>

    @section('page-title', 'Make Payment')
    @push('css')
        <style>
            .table-td-width-50 {
                width: 50% !important;
                white-space: nowrap !important;
                height: auto !important;
                padding: 5px !important;
                line-height: 1.4;
            }
        </style>
    @endpush
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h4 class="header-title">Make Payment</h4>
                    </div>

                    <form action="{{ route('admin.payment.search.stu') }}" method="POST" class="form-prevent">
                        @csrf
                        @method('post')
                        <div class="container">
                            <div class="mx-auto mb-4">

                                <div class="form-group d-flex justify-content-center flex-wrap">
                                    <input type="text" placeholder="Student ID" name="sId"
                                        class="form-control form-control-underlined w-50" autofocus>
                                    <button type="submit"
                                        class="btn btn-primary px-4 rounded-pill btn-block shadow-sm ms-2 form-prevent-multiple-submit">Search</button>
                                </div>

                            </div>
                        </div>
                    </form>


                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>

    @isset($student)
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                        <div class="row">

                            <div class="col-12 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-12 ">
                                        <h4 class="card-title "><b>Student Information</b></h4>
                                        <div class="card border-2 shadow h-80">
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td rowspan="5">
                                                                        <img src="" alt="">

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        class="table-td-width-50 table-primary">
                                                                        <span
                                                                            class="fw-bold td-font-color">Student
                                                                            Name</span><br>
                                                                        {{ $student->user->name }}
                                                                    </td>
                                                                    <td
                                                                        class="table-td-width-50  table-primary">
                                                                        <span
                                                                            class="fw-bold td-font-color">Student
                                                                            ID</span><br>
                                                                        {{ $student->student_id }}

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="table-td-width-50">
                                                                        <span
                                                                            class="fw-bold td-font-color">Class</span><br>
                                                                        {{ $student->schoolClass->name }}
                                                                    </td>
                                                                    <td
                                                                        class="table-td-width-50">
                                                                        <span
                                                                            class="fw-bold td-font-color">Section</span><br>
                                                                        {{ $student->section->name }}
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            @isset($paymentSuccess)
                                <div class="alert alert-success d-flex p-2 rounded-3" role="alert">
                                    <i class="fa fa-check-circle text-success me-3 align-self-center"></i>
                                    <div class="mb-0">
                                        <p class="m-0 p-0 fw-bold">Payment Success!</p>
                                        <p class="m-0 p-0">Last Payment transaction id <b>#{{ $paymentSuccess }}</b> </p>
                                    </div>
                                </div>
                            @endisset

                            @isset($payments)
                                <div class="col-12 col-sm-12 col-md-12 mb-2">
                                    <div class="row">
                                        <h4 class="header-title">Payment Summary</h4>
                                        <div class="col-lg-4 mb-2">

                                            <div class="card h-80 border-2 shadow"
                                                style="border-left-width: 0.40rem !important; border-left-color: #0061f2 !important;">
                                                <div class="card-body">
                                                    <div class="small text-muted">Total Payable:</div>
                                                    <div class="h3">
                                                        {{ number_format($student->schoolClass->total_payment, 2) }}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 mb-2">
                                            <div class="card h-80 border-2 shadow"
                                                style="border-left-color: #00ac69 !important; border-left-width: 0.40rem !important;">
                                                <div class="card-body">
                                                    <div class="small text-muted">Total Paid: </div>
                                                    <div class="h3">{{ number_format($totalPaid, 2) }}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 mb-2">
                                            <div class="card h-80 border-2 shadow"
                                                style="border-left-width: 0.40rem !important;border-left-color: #6900c7 !important;">
                                                <div class="card-body">
                                                    <div class="small text-muted">Dues Leftover:</div>
                                                    <div class="h3 d-flex align-items-center">
                                                        {{ number_format($student->schoolClass->total_payment - $totalPaid, 2) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endisset


                            <div class="col-12 col-md-12 col-sm-12 mb-4">
                                <h4 class="header-title">Add New Payment</h4>

                                <form action="{{ route('admin.payment.store') }}" method="POST">
                                    @csrf
                                    @method('post')
                                    <input type="text" class="disabled hidden" name="student_id"
                                        value="{{ $student->id }}" hidden>
                                    <input type="number" name="amount" required class="form-control mb-2"
                                        placeholder="Enter Amount">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>


                        </div>

                        <div class="row">

                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <h4 class="header-title">Payment History</h4>
                            </div>
                            <div class="col-md-12 col-sm-12 mb-2">
                                <table id="datatable" class="table dt-responsive table-responsive nowrap align-middle">
                                    <thead>
                                        <tr>
                                            <th>#</th>

                                            <th>Amount</th>
                                            <th>Transaction ID</th>
                                            <th>At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        @forelse ($payments as $payment)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>

                                                <td>{{ $payment->amount_paid }}</td>
                                                <td>{{ $payment->transaction_number }}</td>
                                                <td>{{ $payment->created_at->format('d-m-Y') }}</td>
                                                <td>


                                                    <a href="{{ route('admin.payment.delete', ['student_id' => $student->id, 'transactionNumber' => $payment->transaction_number]) }}"
                                                        class="btn btn-sm btn-danger"
                                                        onclick="return confirm('Are you sure you want to delete this?')"
                                                        role="button">Delete</a>

                                                </td>

                                            </tr>

                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">No history Found</td>
                                            </tr>
                                        @endforelse


                                    </tbody>
                                </table>


                            </div>




                        </div>
                    </div>
                </div>
            </div>
        @endisset
        @push('js')
            <script src="{{ asset('backend/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
            <script src="{{ asset('backend/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
            <script src="{{ asset('backend/libs/pdfmake/build/pdfmake.min.js') }}"></script>
            <script src="{{ asset('backend/libs/pdfmake/build/vfs_fonts.js') }}"></script>
            <!-- third party js ends -->

            <!-- Datatables init -->
            <script src="{{ asset('backend/js/pages/datatables.init.js') }}"></script>
        @endpush

</x-backend.layouts.master>
