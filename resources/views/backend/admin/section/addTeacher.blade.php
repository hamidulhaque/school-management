<x-backend.layouts.master>
    @section('page-title', 'students/')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="header-title" style="font-family: Karla, Bangla605, sans-serif;">Teachers List</h4>
                        {{-- <a href="{{ route('admin.student.create') }}" class="btn btn-primary">Add New students</a> --}}
                    </div>

                    <section class="mt-3">
                        <div class="row">
                            <div class="col-12">
                                <h2>Add Teachers to Section: {{ $section->name }},
                                    {{ $section->schoolClass->name }},{{ $section->schoolClass->academic_year }}</h2>

                                <div class="form-group">
                                    <label for="students">Select Teachers</label>
                                    <div class="mb-2">
                                        <button type="button" id="checkAll" class="btn btn-secondary btn-sm">Check
                                            All</button>
                                        <button type="button" id="uncheckAll" class="btn btn-secondary btn-sm">Uncheck
                                            All</button>
                                    </div>
                                </div>

                            </div>
                            <form action="{{ route('admin.section.addTeacherStore', $section->id) }}" class="form-prevent"
                                method="POST">
                                @csrf
                                <table id="datatable"
                                    class="table table-bordered dt-responsive table-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Select</th>
                                            <th>Name</th>
                                            <th>Total Class</th>
                                            <th>Created At</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($teachers->isNotEmpty())

                                            @foreach ($teachers as $teacher)
                                                @php
                                                    $totalSections = $teacher->section->count();
                                                @endphp
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>
                                                        <input class="student-checkbox" type="checkbox"
                                                            name="teachers[]" value="{{ $teacher->id }}">
                                                    </td>

                                                    <td>{{ $teacher->user->name }}</td>

                                                    <td>{{ $totalSections ?? '' }}</td>

                                                    <td>{{ $teacher->created_at }}</td>
                                                </tr>
                                            @endforeach

                                        @endif
                                    </tbody>

                                </table>

                                <button class="btn btn-primary w-100 form-prevent-multiple-submit">Save</button>
                            </form>
                        </div>

                    </section>
                </div>


            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end col -->
    </div>


    @push('js')
        <script src="{{ asset('backend/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('backend/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/pdfmake.min.js') }}"></script>
        <script src="{{ asset('backend/libs/pdfmake/build/vfs_fonts.js') }}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ asset('backend/js/pages/datatables.init.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#checkAll').click(function() {
                    $('.student-checkbox').prop('checked', true);
                });

                $('#uncheckAll').click(function() {
                    $('.student-checkbox').prop('checked', false);
                });
            });
        </script>
    @endpush



</x-backend.layouts.master>
