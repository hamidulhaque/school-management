// logout action
$(document).ready(function () {
    $("#logout-link").on("click", function (e) {
        e.preventDefault();
        $.ajax({
            url: logoutRoute,
            type: "POST",
            data: {
                _token: csrfToken,
            },
            success: function (response) {
                window.location.href = mainRoute;
            },
            error: function (response) {
                console.log("Logout failed");
            },
        });
    });

    // $('.form-prevent').submit(function () {
    //     $(this).find('.form-prevent-multiple-submit').prop('disabled', true).html('Processing...');
    // });

    $(".form-prevent").submit(function () {
        $(this)
            .find(".form-prevent-multiple-submit")
            .prop("disabled", true)
            .html("Processing...");

        return true;
    });
});
