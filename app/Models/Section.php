<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = [
        'name',
        'school_classes_id',
        'created_by',
    ];
    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class, 'sections_teachers', 'section_id', 'teacher_id')->withTimestamps();
    }

    public function schoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'school_classes_id');
    }
}
