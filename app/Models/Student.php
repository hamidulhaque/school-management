<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = [
        'user_id',
        'student_id',
        'class_id',
        'section_id',
        'guardian_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function guardian()
    {
        return $this->belongsTo(Guardian::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }


    public function schoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'class_id');
    }

}
