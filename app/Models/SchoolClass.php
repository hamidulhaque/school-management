<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'academic_year',
        'last_enrollment',
        'total_payment',
        'duration_months',
        'max_student',
        'created_by',
        'start_date'
    ];

    public function sections()
    {
        return $this->belongsToMany(Section::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'class_id');
    }


}
