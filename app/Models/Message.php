<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory,SoftDeletes;

    public function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    public function guardian() {
        return $this->belongsTo(Guardian::class);
    }
}
