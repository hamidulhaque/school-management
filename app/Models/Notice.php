<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Notice extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'content',
        'title',
        'slug',
        'deleted_by',
        'created_by',
    ];


    public function users()
    {
        return $this->belongsToMany(User::class, 'notices_users', 'notice_id', 'user_id')
            ->withPivot('seen', 'seen_at')
            ->withTimestamps();
    }



    public function seenBy()
    {
        return $this->belongsToMany(User::class, 'notices_users')
            ->withPivot('seen', 'seen_at')
            ->withTimestamps();
    }





    public function isSeenByAuthUser()
    {
        $user = Auth::user();

        if (!$user) {
            return false; // No authenticated user
        }

        return $this->users()
            ->where('users.id', $user->id)
            ->wherePivot('seen', true)
            ->exists();
    }


}
