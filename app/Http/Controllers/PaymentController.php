<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Student;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function adminPayment()
    {
        $student = session('student');
        $payments = session('payments');
        $paymentSuccess = session('paymentSuccess');
        $totalPaid = session('totalPaid');
        $success = session('success');
        session()->forget('student');
        session()->forget('payments');
        session()->forget('paymentSuccess');
        session()->forget('totalPaid');
        session()->forget('success');
        return view('backend.admin.payment.index', compact('student', 'payments', 'paymentSuccess', 'totalPaid', 'success'));

    }



    public function adminStuSearch(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'sId' => 'required|exists:students,student_id',
        ], [
            'sId.exists' => 'No student found!',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {

            $student = Student::where('student_id', $request['sId'])->first();

            if (!$student) {
                return redirect()->back()->withErrors(['error' => 'No student found!']);
            }

            $payments = Payment::where('student_id', $student->id)->orderBy('created_at', 'desc')->get();


            $totalPaid = $payments->sum('amount_paid');

            return view('backend.admin.payment.index', compact('student', 'payments', 'totalPaid'));

        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();
        }

    }


    public function adminPaymentStore(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|exists:students,id',
            'amount' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return view('backend.admin.payment.index')->withErrors(['error' => 'Data not validated!']);
        }


        try {
            $student = Student::where('id',$request['student_id'])->first();
            // dd($request->all());
            // dd($student);


            if (!$student) {
                return view('backend.admin.payment.index')->withErrors(['error' => 'Student not found!']);
            }

            $payment = new Payment();
            $payment->student_id = $student->id;
            $payment->transaction_number = $this->generateTransactionNumber();
            $payment->amount_paid = $request->amount;
            $payment->payment_date = now();
            $payment->created_by = Auth::id();
            $payment->save();

            $payments = Payment::where('student_id', $student->id)->orderBy('created_at', 'desc')->get();

            $paymentSuccess = $payment->transaction_number;

            $totalPaid = $payments->sum('amount_paid');

            return redirect()->route('admin.payment.index')
                ->with('student', $student)
                ->with('payments', $payments)
                ->with('paymentSuccess', $paymentSuccess)
                ->with('totalPaid', $totalPaid)
                ->with('success', 'Payment added successfully.');


        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!']);
        }

    }

    protected function generateTransactionNumber()
    {
        do {
            $uniqid = strtoupper(uniqid());
            $randomNumber = mt_rand(1000, 9999);
            $transactionNumber = substr($uniqid . $randomNumber, 0, 12);
        } while (Payment::where('transaction_number', $transactionNumber)->exists());

        return $transactionNumber;
    }

    public function adminPayDelete($student_id, $transactionNumber)
    {

        try {
            $student = Student::find($student_id)->first();

            if (!$student) {
                return view('backend.admin.payment.index')->withErrors(['error' => 'Student not found!']);
            }

            Payment::where('student_id', $student->id)->where('transaction_number', $transactionNumber)->delete();

            $payments = Payment::where('student_id', $student->id)->orderBy('created_at', 'desc')->get();

            $totalPaid = $payments->sum('amount_paid');

            return redirect()->route('admin.payment.index')
                ->with('student', $student)
                ->with('payments', $payments)
                ->with('totalPaid', $totalPaid)
                ->with('success', 'Payment Deleted successfully.');


        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!']);
        }
    }


}
