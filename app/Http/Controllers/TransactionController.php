<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function adminTransactionView(Request $request)
    {
        $search = $request->get('search');
        $limit = $request->get('limit', 5);

        $query = Payment::query();

        if ($search) {
            $query->whereHas('student', function ($q) use ($search) {
                $q->where('student_id', 'like', "%{$search}%");
            })->orWhere('transaction_number', 'like', "%{$search}%");
        }

        $transactions = $query->latest()->paginate($limit);

        return view('backend.admin.transaction.index', compact('transactions', 'limit', 'search'));
    }

}
