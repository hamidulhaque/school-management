<?php

namespace App\Http\Controllers;

use App\Helpers\SlugHelper;
use App\Models\Notice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class NoticeController extends Controller
{

    public function index()
    {
        $notices = Notice::latest()->get();
        return view('backend.admin.notice.index', compact('notices'));
    }



    public function create()
    {
        return view('backend.admin.notice.create');

    }



    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
            'title' => 'required|string|max:100',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {


            $notice = new Notice();
            $notice->title = $request->input('title');
            $notice->content = $request->input('content');
            $notice->slug = SlugHelper::generate(Notice::class, $notice->title);
            $notice->created_by = auth()->id();
            $notice->save();

            return redirect()->route('admin.notice.index')->with('success', 'Notice created successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();
        }
    }



    public function edit(Notice $notice)
    {
        return view('backend.admin.notice.edit', compact('notice'));

    }



    public function update(Request $request, Notice $notice)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'content' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        try {


            if ($notice->title !== $request->input('title')) {
               
                $notice->title = $request->input('title');
                $notice->slug = SlugHelper::generate(Notice::class, $notice->title);
        
            }

            $notice->content = $request->input('content');

            $notice->save();


            return redirect()->route('admin.notice.index')->with('success', 'Notice updated successfully.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['error' => $e->getMessage()])->withInput();

        }

    }



    public function destroy(Notice $notice)
    {
        try {

            $notice->delete();
            return redirect()->back()->with('success', 'Notice deleted successfully.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['error' => 'Failed to delete notice. Please try again.']);
        }
    }



    public function clearNoticeTop(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'notices' => 'required|array',
            'notices.*' => 'exists:notices,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back();
        }

        try {
            $noticeIds = $request->input('notices');
            $userId = auth()->id();


            $notices = Notice::whereIn('id', $noticeIds)
                ->whereDoesntHave('users', function ($query) use ($userId) {
                    $query->where('user_id', $userId)
                        ->where('seen', true);
                })
                ->get();


            if ($notices) {
                foreach ($noticeIds as $noticeId) {
                    $notice = $notices->firstWhere('id', $noticeId);

                    if ($notice) {
                        // Attach user to notice with pivot attributes
                        $notice->users()->syncWithoutDetaching([
                            $userId => [
                                'seen' => true,
                                'seen_at' => now(),
                            ]
                        ]);
                    }
                }
            }


            return redirect()->back();


        } catch (\Exception $e) {

            return redirect()->back()->with(['error' => $e->getMessage()]);

        }
    }



    public function noticeViewSingle($slug)
    {
        try {
            $notice = Notice::where('slug', $slug)->first();
            if (!$notice) {
                abort(404);
            }
            $userId = auth()->id();
            $notice->users()->syncWithoutDetaching([
                $userId => [
                    'seen' => true,
                    'seen_at' => now(),
                ]
            ]);
            return view('backend.admin.notice.single', compact('notice'));
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!']);
        }
    }



    public function noticeViewAll()
    {
        try {
            $notices = Notice::paginate(10);
            return view('backend.admin.notice.all', compact('notices'));
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!']);
        }
    }


}
