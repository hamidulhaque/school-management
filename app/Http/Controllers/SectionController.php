<?php

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Models\Section;
use Validator;

class SectionController extends Controller
{

    public function index()
    {
        $sections = Section::latest()->get();
        return view('backend.admin.section.index', compact('sections'));
    }

    public function create()
    {
        $classes = SchoolClass::latest()->get();
        return view('backend.admin.section.create', compact('classes'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'class_id' => 'required|exists:school_classes,id',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        try {
            $section = new Section();
            $section->name = $request->input('name');
            $section->school_classes_id = $request->input('class_id');
            $section->created_by = auth()->id();
            $section->save();

            return redirect()->back()->with(['success' => 'Section created successfully!']);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();
        }


    }

    public function edit(Section $section)
    {
        $classes = SchoolClass::latest()->get();

        return view('backend.admin.section.edit', compact('section', 'classes'));
    }


    public function update(Request $request, Section $section)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'class_id' => 'required|exists:school_classes,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $section->update([
                'name' => $request->input('name'),
                'school_classes_id' => $request->input('class_id'),
            ]);

            return redirect()->back()->with(['success' => 'Section updated successfully!']);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();
        }
    }


    public function destroy(Section $section)
    {
        try {
            $section->delete();
            return redirect()->back()->with('success', 'Section deleted successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Failed to delete section. Please try again.');
        }
    }



    public function addStu(Section $section)
    {
        $students = Student::where('class_id', $section->school_classes_id)
            ->whereNull('section_id')
            ->get();


        return view('backend.admin.section.addStudent', compact('section', 'students'));
    }


    public function addStuStore(Section $section, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'students' => 'required|array',
            'students.*' => 'exists:students,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $studentIds = $request->input('students');


            Student::whereIn('id', $studentIds)
                ->update(['section_id' => $section->id]);


            $year = date('Y');
            $month = date('m');
            foreach ($studentIds as $studentId) {
                $student = Student::findOrFail($studentId);
                if (empty($student->student_id)) {
                    $student->student_id = generateStudentId($year, $month);
                    $student->save();
                }
            }
            return redirect()->back()->with(['sucess' => 'Student added successfully!']);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();

        }
    }


    public function addTeacher(Section $section)
    {
        $assignedTeacherIds = $section->teachers->pluck('id');
        $teachers = Teacher::whereNotIn('id', $assignedTeacherIds)->latest()->get();

        return view('backend.admin.section.addTeacher', compact('section', 'teachers'));
    }


    public function addTeacherStore(Section $section, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teachers' => 'required|array',
            'teachers.*' => 'exists:teachers,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $teacherIds = $request->input('teachers');


            $pivotData = [];
            foreach ($teacherIds as $teacherId) {
                $pivotData[$teacherId] = [
                    'assign_date' => now(),
                    'created_by' => auth()->id(),
                ];
            }
            $section->teachers()->attach($pivotData);

            return redirect()->back()->with('success', 'Teachers added to the section successfully!');


        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong!'])->withInput();

        }
    }


}
