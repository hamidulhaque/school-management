<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuardianDashboardController extends Controller
{
    public function dashboard()
    {
        return view('backend.guardian.dashboard');
    }
}
