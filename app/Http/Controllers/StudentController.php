<?php

namespace App\Http\Controllers;

use App\Models\Guardian;
use App\Models\SchoolClass;
use App\Models\Section;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Validator, Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    protected const GUARDIAN_ROLE_ID = 3;
    protected const STUDENT_ROLE_ID = 4;



    public function adminStudentAppList(Request $request)
    {
        $search = $request->get('search');
        $limit = $request->get('limit', 5);
        $students = Student::whereNull('student_id')->whereNull('section_id')->latest()->paginate($limit);
        return view('backend.admin.student.approve', compact('students', 'limit', 'search'));
    }

    public function adminStuCreate()
    {

        $currentAcademicYear = now()->year;
        $today = now()->toDateString();


        $classes = SchoolClass::where('academic_year', '>=', $currentAcademicYear)
            ->whereDate('last_enrollment', '<=', $today)
            ->whereRaw('(SELECT COUNT(*) FROM students WHERE students.class_id = school_classes.id) < max_student')
            ->get();


        $sections = Section::all();

        $studentCredentials = session('studentCredentials');
        $guardianCredentials = session('guardianCredentials');
        $success = session('success');

        session()->forget(['studentCredentials', 'guardianCredentials']);

        return view('backend.admin.student.create', compact('classes', 'sections', 'studentCredentials', 'guardianCredentials', 'success'));
    }



    public function adminStuStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_name' => 'required|string|max:255',
            'class_id' => 'required|integer|exists:school_classes,id',
            'section_id' => 'nullable|integer|exists:sections,id',
            'guardian_name' => 'required|string|max:255',
            'guardian_phone' => 'required|string|max:15',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            DB::transaction(function () use ($request) {
                $existingGuardian = Guardian::where('phone', $request->guardian_phone)->first();

                if ($existingGuardian) {
                    $guardian = $existingGuardian;
                    $guardianUser = $existingGuardian->user;
                } else {
                    $guardianPassword = Str::random(8);
                    $guardianUser = User::create([
                        'role_id' => self::GUARDIAN_ROLE_ID,
                        'name' => $request->guardian_name,
                        'phone' => $request->guardian_phone,
                        'password' => Hash::make($guardianPassword),
                    ]);

                    $guardian = Guardian::create([
                        'user_id' => $guardianUser->id,
                        'phone' => $request->guardian_phone,
                    ]);
                }

                $studentPassword = Str::random(8);
                $studentUser = User::create([
                    'role_id' => self::STUDENT_ROLE_ID,
                    'name' => $request->student_name,
                    'password' => Hash::make($studentPassword),
                ]);

                $student = Student::create([
                    'user_id' => $studentUser->id,
                    'student_id' => 'S' . time(),
                    'class_id' => $request->class_id,
                    'section_id' => $request->section_id,
                    'guardian_id' => $guardian->id,
                ]);

                $studentCredentials = [
                    'name' => $studentUser->name,
                    'student_id' => $student->student_id,
                    'username' => $studentUser->phone ?? $studentUser->email,
                    'password' => $studentPassword,
                ];

                $guardianCredentials = [
                    'name' => $guardianUser->name,
                    'username' => $guardianUser->phone ?? $guardianUser->email,
                    'password' => $existingGuardian ? 'Already Exists' : $guardianPassword,
                ];


                session()->flash('success', 'Student Created Successfully!');
                session()->flash('studentCredentials', $studentCredentials);
                session()->flash('guardianCredentials', $guardianCredentials);
            });


            return redirect()->route('admin.student.create')->with('success', 'Student Created!');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Something went wrong! Please try again.']);
        }
    }


}
