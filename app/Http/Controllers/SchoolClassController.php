<?php

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class SchoolClassController extends Controller
{
    public function classes()
    {
        $classes = SchoolClass::latest()->get();
        return view('backend.admin.class.index', compact('classes'));
    }


    public function create()
    {
        return view('backend.admin.class.create');
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'academic_year' => 'required|digits:4|integer|min:2000|max:' . (date('Y') + 1),
            'last_enrollment' => 'required|date',
            'total_payment' => 'required|numeric|min:0',
            'duration_months' => 'required|integer|min:1',
            'max_student' => 'nullable|integer|min:1',
            'start_date' => 'required|date',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            SchoolClass::create([
                'name' => $request->name,
                'academic_year' => $request->academic_year,
                'last_enrollment' => $request->last_enrollment,
                'total_payment' => $request->total_payment,
                'duration_months' => $request->duration_months,
                'max_student' => $request->max_student,
                'start_date' => $request->start_date,
                'created_by' => Auth::id(),
            ]);

            return redirect()->back()->with(['success' => 'Class successfully created!']);
        } catch (\Exception $e) {
            return redirect()->back()->withError(['error' => 'Something Went Wrong!']);
        }

    }


    public function edit(SchoolClass $class)
    {
        return view('backend.admin.class.edit', compact('class'));
    }


    public function update(Request $request, SchoolClass $class)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'academic_year' => 'required|digits:4|integer|min:2000|max:' . (date('Y') + 1),
            'last_enrollment' => 'required|date',
            'total_payment' => 'required|numeric|min:0',
            'duration_months' => 'required|integer|min:1',
            'max_student' => 'nullable|integer|min:1',
            'start_date' => 'required|date',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $class->update([
                'name' => $request->name,
                'academic_year' => $request->academic_year,
                'last_enrollment' => $request->last_enrollment,
                'total_payment' => $request->total_payment,
                'duration_months' => $request->duration_months,
                'max_student' => $request->max_student,
                'start_date' => $request->start_date,
            ]);

            return redirect()->back()->with(['success' => 'Class successfully updated!']);
        } catch (\Exception $e) {
            return redirect()->back()->withError(['error' => 'Something Went Wrong!']);
        }
    }


    public function destroy(SchoolClass $class)
    {
        try {
            $class->delete();
            return redirect()->back()->with(['success' => 'Class deleted successfully']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => 'Failed to delete class']);
        }
    }


}
