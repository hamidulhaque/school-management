<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomLoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        return view('frontend.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'identifier' => 'required|string',
            'password' => 'required|string',
        ]);

        $identifier = $request->input('identifier');
        $password = $request->input('password');
        $remember = $request->filled('remember');

        if ($this->attemptLogin($identifier, $password, $remember)) {
            return $this->handleUserRedirect();
        }

        return redirect()->back()->withInput($request->only('identifier'))->withErrors([
            'identifier' => 'These credentials do not match our records.',
        ]);
    }

    // protected function attemptLogin($identifier, $password)
    // {
    //     if (filter_var($identifier, FILTER_VALIDATE_EMAIL)) {
    //         return Auth::attempt(['email' => $identifier, 'password' => $password]);
    //     } elseif (is_numeric($identifier) && strlen($identifier) == 11) {
    //         return Auth::attempt(['phone' => $identifier, 'password' => $password]);
    //     } else {

    //         $student = Student::where('student_id', $identifier)->first();
    //         if ($student) {
    //             return Auth::attempt(['id' => $student->user_id, 'password' => $password]);
    //         }
    //         return false;
    //     }
    // }
    protected function attemptLogin($identifier, $password, $remember)
    {
        if (filter_var($identifier, FILTER_VALIDATE_EMAIL)) {
            return Auth::attempt(['email' => $identifier, 'password' => $password], $remember);
        } elseif (is_numeric($identifier) && strlen($identifier) == 11) {
            return Auth::attempt(['phone' => $identifier, 'password' => $password], $remember);
        } else {
            $student = Student::where('student_id', $identifier)->first();
            if ($student) {
                return Auth::attempt(['id' => $student->user_id, 'password' => $password], $remember);
            }
            return false;
        }
    }
    protected function handleUserRedirect()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $redirectUrl = session('redirect_url');
            session()->forget('redirect_url');
            return redirect()->intended($redirectUrl ?? route('admin.dashboard'));

        } elseif ($user->hasRole('student')) {
            $redirectUrl = session('redirect_url');
            session()->forget('redirect_url');
            return redirect()->intended($redirectUrl ?? route('student.dashboard'));

        } elseif ($user->hasRole('teacher')) {
            $redirectUrl = session('redirect_url');
            session()->forget('redirect_url');
            return redirect()->intended($redirectUrl ?? route('teacher.dashboard'));

            // return redirect()->route('teacher.dashboard');
        } elseif ($user->hasRole('guardian')) {
            $redirectUrl = session('redirect_url');
            session()->forget('redirect_url');
            return redirect()->intended($redirectUrl ?? route('guardian.dashboard'));

            // return redirect()->route('guardian.dashboard');
        } else {
            Auth::logout();
            return redirect('/login')->withErrors(['identifier' => 'Role not defined.']);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
