<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\Notice;


class ShareTopNotices
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = Auth::user();
        $topNotices = Notice::latest()->take(10)->get();
        $unseenCount = 0;

        if ($user && $topNotices) {
            foreach ($topNotices as $notice) {
                if (!$notice->isSeenByAuthUser()) {
                    $unseenCount++;
                }
            }
        }

        View::share([
            'topNotices' => $topNotices,
            'unseenNoticeCount' => $unseenCount,
        ]);
        return $next($request);
    }
}
