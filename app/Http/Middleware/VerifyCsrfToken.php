<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Support\Facades\Redirect;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        //
    ];
    public function handle($request, \Closure $next)
    {
        try {
            return parent::handle($request, $next);
        } catch (\Illuminate\Session\TokenMismatchException $e) {
            session(['redirect_url' => url()->current()]);
            return redirect()->route('login')->withErrors(['message' => 'Your session has expired. Please login again.']);
        }
    }
    protected function handleTokenMismatch($request, $exception)
    {
        return Redirect::guest(route('login'))->withErrors(['error' => 'Your session has expired. Please log in again.']);
    }
}
