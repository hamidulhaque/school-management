<?php

use App\Models\Student;

/**
 * Generate a unique student ID based on the year, month, and sequence number.
 *
 * @param int $year
 * @param int $month
 * @return string
 */
function generateStudentId($year, $month)
{
    $prefix = substr($year, -2) . sprintf('%02d', $month);


    $maxId = Student::where('id', 'like', $prefix . '%')->max('id');

    $sequence = intval(substr($maxId, -3)) + 1;

    return $prefix . sprintf('%03d', $sequence);
}


