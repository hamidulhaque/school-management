<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Student;
use App\Models\Guardian;
use App\Models\Teacher;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::firstOrCreate(['name' => 'admin']);
        $studentRole = Role::firstOrCreate(['name' => 'student']);
        $guardianRole = Role::firstOrCreate(['name' => 'guardian']);
        $teacherRole = Role::firstOrCreate(['name' => 'teacher']);

        // Create an admin user
        $admin = User::create([
            'role_id' => $adminRole->id,
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
        ]);
        
        // Create a student user
        $studentUser = User::create([
            'role_id' => $studentRole->id,
            'name' => 'Student User',  
            'email' => '',
            'password' => bcrypt('password'),
        ]);

        $student = Student::create([
            'user_id' => $studentUser->id,
            'student_id' => '2401001',
        ]);

        // Create a guardian user
        $guardianUser = User::create([
            'role_id' => $guardianRole->id,
            'name' => 'Guardian User',
            'phone' => '01845079509',
            'password' => bcrypt('password'),
        ]);

        $guardian = Guardian::create([
            'user_id' => $guardianUser->id,
            'phone' => '01845079509',
        ]);

        // Link the student to the guardian
        $student->guardian_id = $guardian->id;
        $student->save();

        // Create a teacher user
        $teacherUser = User::create([
            'role_id' => $teacherRole->id,
            'name' => 'Teacher User',
            'email' => 'teacher@example.com',
            'password' => bcrypt('password'),
        ]);

        $teacher = Teacher::create([
            'user_id' => $teacherUser->id,
        ]);
    }
}
